import sys
import os
import easysnmp
import re
from dotenv import load_dotenv
from easysnmp import Session
from sqlalchemy import Column, String, Integer, create_engine, and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import DatabaseError
from typing import Dict, List, Tuple, NewType
import xml.etree.ElementTree as ET

load_dotenv()

# Global variables
FILE_STORE_ERROR = "FAILD: The Device's information already exist into File"
DB_STORE_ERROR = "FAILD: The Device's information already exist into DB"
BOTH_STORE_ERROR = "FAILD: The Device's information already exist in both storage medium"
SUCCESS_STORE_DB = "SUCCESS: New device info stored in DB"
SUCCESS_STORE_FILE = "SUCCESS: New device info stored in File"

OID = "1.3.6.1.2.1.1.1.0"

FILENAME = os.getenv('FILE_NAME')

DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_NAME = os.getenv('DB_NAME')
DB_ADDRESS = os.getenv('DB_ADDRESS')

IP_EXPRESSION = '''^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)$'''


class SNMPError(Exception):
    pass


class IPError(Exception):
    pass


Base = declarative_base()


class CableModemModel(Base):
    __tablename__ = "cm_models"
    id = Column(Integer(), primary_key=True)
    vendor = Column(String(100), nullable=False)
    model = Column(String(1000), nullable=False)
    softversion = Column(String(100), nullable=False)

    def __str__(self):
        return self.vendor

    def __init__(self, vendor: str, model: str, softversion: str):
        self.vendor = vendor
        self.model = model
        self.softversion = softversion

    @classmethod
    def find_cable_modem_db(cls, cm) -> int:
        vendor = cm.vendor
        model = cm.model
        softversion = cm.softversion
        cable_modem = session.query(cls).filter(and_(
            cls.vendor == vendor, cls.model == model, cls.softversion == softversion)).first()
        if cable_modem != None:
            return 1
        else:
            return 0

    def find_cable_modem_file(self, filename: str) -> int:
        tree = ET.parse(filename)
        root = tree.getroot()
        for child in root:
            vendor = child[0].text
            model = child[1].text
            softversion = child[2].text
            data = [self.vendor, self.model, self.softversion]
            if data == [vendor, model, softversion]:
                return 1
        return 0

    def save_to_db(self):
        session.add(self)
        session.commit()

    def save_to_file(self, filename: str) -> None:
        tree = ET.parse(filename)

        # Root-Element
        root = tree.getroot()

        # Child
        cm = ET.Element("cm_model")
        root.append(cm)

        # Sub-Elements
        vendor = ET.SubElement(cm, "vendor")
        vendor.text = self.vendor
        model = ET.SubElement(cm, "model")
        model.text = self.model
        softversion = ET.SubElement(cm, "softversion")
        softversion.text = self.softversion

        tree = ET.ElementTree(root)

        with open(filename, "wb") as files:
            tree.write(files)


def get_modem_data(ip: str, OID: str):

    session_snmp = Session(hostname=ip, community='private', version=2)

    try:
        description = session_snmp.get(OID)
    except:
        raise SNMPError

    information = dict()

    for item in description.value.split(";"):
        info, value = item.split(":")
        info = info.strip().strip("<<")
        value = value.strip().strip(">>")
        information[info] = value

    cm_obj = CableModemModel(
        information['VENDOR'], information['MODEL'], information['SW_REV'])

    return cm_obj


def make_xml_file(filename: str) -> None:
    root = ET.Element("cm_models")
    tree = ET.ElementTree(root)

    with open(filename, "wb") as files:
        tree.write(files)


def check_ipv4(IP: str) -> None:
    if re.search(IP_EXPRESSION, IP) is None:
        raise IPError


if __name__ == '__main__':

    # Input Arguments
    try:
        arguments = sys.argv[1:]
        IP = arguments[0]
        STORAGE = arguments[1]
        check_ipv4(IP)
    except IndexError:
        raise SystemExit(f"Argument Error {sys.argv[0]}")
    except IPError:
        raise SystemExit(f"The IP {sys.argv[1]} is invalid")

    try:
        engine = create_engine(
            f"mysql+pymysql://{DB_USER}:{DB_PASSWORD}@{DB_ADDRESS}/{DB_NAME}?charset=utf8mb4")
        # open session base de datos
        Session_db = sessionmaker(engine)
        session = Session_db()

    except:
        print("Error: Conection with DataBase")

    # Check if the file exist
    if not os.path.isfile(FILENAME):
        make_xml_file(FILENAME)

    # Check uf the table exist
    if not engine.dialect.has_table(engine, "cm_models"):
        Base.metadata.create_all(engine)

    # Ask information from the device
    try:
        cm_info = get_modem_data(IP, OID)
    except SNMPError:
        raise SystemError("SNMP Error")

    if STORAGE == "db":
        exist_db = CableModemModel.find_cable_modem_db(cm_info)
        if not exist_db:
            cm_info.save_to_db()
            print(SUCCESS_STORE_DB)
        else:
            print(DB_STORE_ERROR)

    elif STORAGE == "file":
        exist_file = cm_info.find_cable_modem_file(FILENAME)
        if not exist_file:
            cm_info.save_to_file(FILENAME)
            print(SUCCESS_STORE_FILE)
        else:
            print(FILE_STORE_ERROR)

    elif STORAGE == "both":

        exist_db = CableModemModel.find_cable_modem_db(cm_info)
        exist_file = cm_info.find_cable_modem_file(FILENAME)

        cases = {
            "11": BOTH_STORE_ERROR,
            "01": FILE_STORE_ERROR,
            "10": DB_STORE_ERROR,
        }

        if not exist_db and not exist_file:
            cm_info.save_to_db()
            cm_info.save_to_file(FILENAME)
        else:
            case = str(exist_db) + str(exist_file)
            print(cases[case])
    else:
        print(f"Bad argument option: {STORAGE}")
