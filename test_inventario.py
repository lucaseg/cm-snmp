from inventario import check_ipv4, make_xml_file, get_modem_data, CableModemModel
import pytest
import os

class IPError(Exception):
    pass


def test_check_ip():
    ips = ["192.168.3333.1",
           "999.99.99.99",
           "1000.34.3.5.5",
           "9192.1923.4949.33",
           "abs.rewqt.tryw.32",
           "sldgoepwmtmh",
           "234232323523523"]
    for ip in ips:
        with pytest.raises(Exception):
            assert check_ipv4(ip)

    ips_valid = ["192.168.0.1",
                "10.10.10.10",
                "200.196.200.65",
                "1.1.1.1"]
    for ip in ips_valid:
        assert check_ipv4(ip) == None

def test_make_xml_file():
    FILENAME = "prueba.xml"
    make_xml_file(FILENAME)

    assert True if os.path.isfile(FILENAME) else False
    os.remove(FILENAME)

def test_get_modem_data():
    ip = "186.19.62.63"
    OID = "1.3.6.1.2.1.1.1.0"
    output = get_modem_data(ip, OID)

    assert isinstance(output, CableModemModel)

def test_get_modem_data_exeption():
    ip = "192.168.1.1"
    OID = "1.3.6.1.2.1.1.1.0"

    with pytest.raises(Exception):
        assert get_modem_data(ip, OID)