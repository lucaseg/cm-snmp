# Examen de programacion Stech

## Objetivo
Realizar una aplicacion que por medio de CLI se solicite informacion a los CableModem y esta sea almacenada en una base de datos o en un archivo.

## Preparacion del entorno
Crear un entorno virtual python

    virtualenv -p python3 venv
Correr el entorno

    source venv/bin/activate
Instalar las dependencias

    pip install -r requirements.txt 
Levantar el contenedor con la base de datos MySql.
    
    docker-compose up -d

Nota: dentro del directorio se encuentra un archivo .env donde estan las variables de entorno utilizadas para el proyecto.

## Ejemplo de uso
Dentro el directorio se encuentra un archivo llamado inventario.py este es el que ejecuta la aplicacion

    python inventario.py 186.19.62.63 db
    python inventario.py 186.19.62.63 file
    python inventario.py 186.19.62.63 both
